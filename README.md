# MAGIC script
Calculate Area Under the Curve (AUC) for physiological data monitoring in R

Script available for non apparied data without missing data.

For apparied data, a version 2 of this script will be realized next.

There is no commercial interest but other R packages are used, please cite the different packages listed below.


----------------------------------------------------------------

__References:__

John Fox and Sanford Weisberg (2011). An {R} Companion to Applied Regression, Second Edition. Thousand
  Oaks CA: Sage. URL: http://socserv.socsci.mcmaster.ca/jfox/Books/Companion
  
Patrick Giraudoux (2017). pgirmess: Data Analysis in Ecology. R package version 1.6.7.
	https://CRAN.R-project.org/package=pgirmess
  
Russell V. Lenth (2016). Least-Squares Means: The R Package lsmeans. Journal of Statistical Software, 69(1), 1-33. 
	doi:10.18637/jss.v069.i01

Spencer Graves, Hans-Peter Piepho and Luciano Selzer with help from Sundar Dorai-Raj (2015). multcompView: Visualizations of Paired Comparisons. R package version 0.1-7. 
	https://CRAN.R-project.org/package=multcompView

-----------------------------------------------------------------


License

See the [LICENSE](https://gitlab.com/RipollJ/MAGIC/blob/master/License.md) file for license rights and limitations (MIT).
