########################################################################
### Analyses by Area Under the Curve on time physiological data set ####
########################################################################

## Created: Nov-2015
## Author: Ripoll
## JobPosition at creation: PhD Student INRA

## AUC method: audpci+1= ((yi + yi+1)*(ti+1 - ti))/2
## do not support missing data (NA)

###########################################################################
## Load library & functions
ilist<-c("lsmeans","multcompView","car","pgirmess")
if (!require(ilist)) install.packages(ilist)
library(lsmeans)
library(multcompView) ## group comparison
library(car)
library(pgirmess)

## Confidence Interval Function
int.ech = function(vector,proba=5) {               
  s = var(vector)
  n = length(vector)
  ddl = n - 1
  proba = (100-proba/2)/100
  t_student = qt(proba, ddl)
  intervalle = t_student * sqrt(s/n)
  return(intervalle)
}

# Standard Error Function
SE<-function(data){
  sterr<- sqrt(var(data,na.rm=TRUE)/ length(data[is.na(data)!=TRUE]))
  return(sterr)
}

# Error bar for plot
error.bar <- function(x, y, upper, lower=upper, length=0.1,...){
  if(length(x) != length(y) | length(y) !=length(lower) | length(lower) != length(upper))
    stop("vectors must be same length")
  arrows(x,y+upper, x, y-lower, angle=90, code=3, length=length, ...)
}

#############################################################################
## define repertories
home="\\\\nas1-avi\\PATH\\User_documents"
dossier="\\\\nas1-avi\\ATH\\User_documents\\PhysioData"
setwd(dossier)

## load dataframes
temp = list.files(pattern="*.txt")
for (i in 1:length(temp)) assign(temp[i], read.delim(temp[i]))

##############################################################################
## Loop for AUC calculations

# Export results of the loop
sink(file="Results.txt", append = F)

## create empty temporary file for loop
auc1_all=data.frame()
auct1_all=data.frame()
auct2_all=data.frame()
auctot_all=data.frame()

## Add dataframes of Environment to a list
listEnv<-ls() 
print(listEnv)

## Start loop
i=1
while(i<=length(listEnv)){
  print(listEnv[i])
  
  data <-read.delim(listEnv[i], dec=",")  # define your decimal before launch
  DI1<-data$DI1 # define Factors Data i.e. period name DI: deficit RP: recovery
  RP1<-data$RP1
  DI2<-data$DI2
  RP2<-data$RP2
  DI3<-data$DI3
  Genotype<-data$Genotype # define your species
  Traitement<-data$Traitement # define your treatment i.e. Stress and Control plants
  A=15 # define time interval during DI and RP period
  B=18
  C=10
  D=21
  #interaction.plot(Genotype, Traitement, DI1) # if you want to look at data
  #interaction.plot(Genotype, Traitement, RP1)
  #interaction.plot(Genotype, Traitement, DI2)
  #interaction.plot(Genotype, Traitement, RP2)
  #interaction.plot(Genotype, Traitement, DI3)
  # calcul AUC
  auc1<-(((DI1+RP1)*A)/2)  
  auc2<-(((RP1+DI2)*B)/2)  
  auc3<-(((DI2+RP2)*C)/2)  
  auc4<-(((RP2+DI3)*D)/2)  
  auct1<-auc1+auc2
  auct2<-auc1+auc2+auc3
  auctot<-auc1+auc2+auc3+auc4         

  if (length(auc1)!=80) print(paste("ERREUR",listEnv[i]," ")) # error search, dependant on number of data in a dataframe
  
  if (length(auc1_all)==0)   auc1_all  =data.frame(auc1)   else auc1_all  =cbind(auc1_all  ,auc1)
  if (length(auct1_all)==0)  auct1_all =data.frame(auct1)  else auct1_all =cbind(auct1_all ,auct1)
  if (length(auct2_all)==0)  auct2_all =data.frame(auct2)  else auct2_all =cbind(auct2_all ,auct2)
  if (length(auctot_all)==0) auctot_all=data.frame(auctot) else auctot_all=cbind(auctot_all,auctot)
  
  ## Mean and error standard by treatment and period
  X0.mean<-tapply(DI1,list(Traitement,Genotype), mean) # first point
  X1.mean<-tapply(auc1,list(Traitement,Genotype), mean) # first part of AUC time curve
  X2.mean<-tapply(auc2,list(Traitement,Genotype), mean)
  X3.mean<-tapply(auc3,list(Traitement,Genotype), mean)
  X4.mean<-tapply(auc4,list(Traitement,Genotype), mean)
  X5.mean<-tapply(auct1,list(Traitement,Genotype), mean)
  X6.mean<-tapply(auct2,list(Traitement,Genotype), mean)
  X7.mean<-tapply(auctot,list(Traitement,Genotype), mean) # from the first part to the last part = ALL AUC
  print(X0.mean)
  print(X1.mean)
  print(X5.mean)
  print(X6.mean)
  print(X7.mean)
  
  X0.SE<-tapply(DI1,list(Traitement,Genotype), SE)
  X1.SE<-tapply(auc1,list(Traitement,Genotype), SE)
  X2.SE<-tapply(auc2,list(Traitement,Genotype), SE)
  X3.SE<-tapply(auc3,list(Traitement,Genotype), SE)
  X4.SE<-tapply(auc4,list(Traitement,Genotype), SE)
  X5.SE<-tapply(auct1,list(Traitement,Genotype), SE)
  X6.SE<-tapply(auct2,list(Traitement,Genotype), SE)
  X7.SE<-tapply(auctot,list(Traitement,Genotype), SE)
  print(X0.SE)
  print(X1.SE)
  print(X5.SE)
  print(X6.SE)
  print(X7.SE)
  
  ## Total AUC plot
  nomfigure=gsub(".txt",".png",listEnv[i])
  png(filename=nomfigure)
  
  barx<-barplot(X7.mean, beside=TRUE,density=c(0,30), col=1,ylab="Total AUC", ylim= c(0,max(X7.mean+X7.SE)))
  error.bar(barx,X7.mean,X7.SE)  
  legend("top", legend =c("Stress","Control"),density=c(0,30), col=1,cex=1,bty="n",ncol=2,xjust=0.5)
  
  dev.off()
    
  ## Combine all data to see AUC evolution
  data2<-cbind(data,auc1,auct1,auct2,auctot)
   
  ## Stats differences by treatment stress and control 
  R1<-aov(DI1~Genotype+Traitement+Genotype*Traitement,data=data)      
  aov1<-anova(R1)
  print(aov1)
  ## analyse rsidus
  res<-residuals(R1)
  hist(res,col="grey", main="Histogramme des rsidus", ylab="Frquence", xlab="valeurs des rsidus")
  print(shapiro.test(res))
  print(leveneTest(res~Genotype*Traitement))
  P<-shapiro.test(res)$p.value
  Leven<-leveneTest(res~Genotype*Traitement)
  P2<-Leven$Pr(>F)`[1]
  if(P>0.05 & P>0.05) { print(test=lsmeans(R1, "Traitement", by="Genotype")
  pairs(test) ## comparaison de type Tukey
  print(cld(test,alpha=0.05)))}
  else { data$ID<-interaction(data$Genotype, data$Traitement)
  KW<-kruskal.test(DI1~data$ID)
  P3<-KW$p.value
  if(P3<0.05){ print(kruskalmc(DI1~data$ID, data=NULL, probs=0.05)}
  else {cat("no differences between conditions at time 0")}
  }
  #######################################
  R1<-aov(auc1~Genotype+Traitement+Genotype*Traitement,data=data2)      
  aov1<-anova(R1)
  print(aov1)
  ## analyse rsidus
  res<-residuals(R1)
  hist(res,col="grey", main="Histogramme des rsidus", ylab="Frquence", xlab="valeurs des rsidus")
  print(shapiro.test(res))
  print(leveneTest(res~Genotype*Traitement))
  P<-shapiro.test(res)$p.value
  Leven<-leveneTest(res~Genotype*Traitement)
  P2<-Leven$Pr(>F)`[1]
  if(P>0.05 & P>0.05) { print(test=lsmeans(R1, "Traitement", by="Genotype")
  pairs(test) ## comparaison de type Tukey
  print(cld(test,alpha=0.05)))}
  else { data$ID<-interaction(data$Genotype, data$Traitement)
  KW<-kruskal.test(auc1~data$ID)
  P3<-KW$p.value
  if(P3<0.05){ print(kruskalmc(auc1~data$ID, data=NULL, probs=0.05)}
  else {cat("no differences between conditions for the first step of AUC")}
  }
  ####################################
  R1<-aov(auct1~Genotype+Traitement+Genotype*Traitement,data=data2)      
  aov1<-anova(R1)
  print(aov1)
  ## analyse rsidus
  res<-residuals(R1)
  hist(res,col="grey", main="Histogramme des rsidus", ylab="Frquence", xlab="valeurs des rsidus")
  print(shapiro.test(res))
  print(leveneTest(res~Genotype*Traitement))
  P<-shapiro.test(res)$p.value
  Leven<-leveneTest(res~Genotype*Traitement)
  P2<-Leven$Pr(>F)`[1]
  if(P>0.05 & P>0.05) { print(test=lsmeans(R1, "Traitement", by="Genotype")
  pairs(test) ## comparaison de type Tukey
  print(cld(test,alpha=0.05)))}
  else { data$ID<-interaction(data$Genotype, data$Traitement)
  KW<-kruskal.test(auct1~data$ID)
  P3<-KW$p.value
  if(P3<0.05){ print(kruskalmc(auct1~data$ID, data=NULL, probs=0.05)}
  else {cat("no differences between conditions until step 2 of AUC")}
  }
  ###################################
  R1<-aov(auct2~Genotype+Traitement+Genotype*Traitement,data=data2)      
  aov1<-anova(R1)
  print(aov1)
  ## analyse rsidus
  res<-residuals(R1)
  hist(res,col="grey", main="Histogramme des rsidus", ylab="Frquence", xlab="valeurs des rsidus")
  print(shapiro.test(res))
  #print(bartlett.test(data,residus))
  print(leveneTest(res~Genotype*Traitement))
  P<-shapiro.test(res)$p.value
  Leven<-leveneTest(res~Genotype*Traitement)
  P2<-Leven$Pr(>F)`[1]
  if(P>0.05 & P>0.05) { print(test=lsmeans(R1, "Traitement", by="Genotype")
  pairs(test) ## comparaison de type Tukey
  print(cld(test,alpha=0.05)))}
  else { data$ID<-interaction(data$Genotype, data$Traitement)
  KW<-kruskal.test(auct2~data$ID)
  P3<-KW$p.value
  if(P3<0.05){ print(kruskalmc(auct2~data$ID, data=NULL, probs=0.05)}
  else {cat("no differences between conditions until step 3 of AUC")}
  }
  ##############################
  R1<-aov(auctot~Genotype+Traitement+Genotype*Traitement,data=data2)      
  aov1<-anova(R1)
  print(aov1)
  ## analyse rsidus
  res<-residuals(R1)
  hist(res,col="grey", main="Histogramme des rsidus", ylab="Frquence", xlab="valeurs des rsidus")
  print(shapiro.test(res))
  print(leveneTest(res~Genotype*Traitement))
  P<-shapiro.test(res)$p.value
  Leven<-leveneTest(res~Genotype*Traitement)
  P2<-Leven$Pr(>F)`[1]
  if(P>0.05 & P>0.05) { print(test=lsmeans(R1, "Traitement", by="Genotype")
  pairs(test) ## comparaison de type Tukey
  print(cld(test,alpha=0.05)))}
  else { data$ID<-interaction(data$Genotype, data$Traitement)
  KW<-kruskal.test(auctot~data$ID)
  P3<-KW$p.value
  if(P3<0.05){ print(kruskalmc(auctot~data$ID, data=NULL, probs=0.05)}
  else {cat("no differences between conditions for total AUC")}
  }
  
   
  
  i=i+1
}

sink() ## END of export Results

# Export tables of AUC steps values
names(auc1_all)=listEnv
names(auct1_all)=listEnv
names(auct2_all)=listEnv
names(auctot_all)=listEnv
write.table(auc1_all,file="auc1 fluo.txt",append=F,row.names=F,col.names=T,quote=F)
write.table(auct1_all,file="auct1 fluo.txt",append=F,row.names=F,col.names=T,quote=F)
write.table(auct2_all,file="auct2 fluo.txt",append=F,row.names=F,col.names=T,quote=F)
write.table(auctot_all,file="auctot fluo.txt",append=F,row.names=F,col.names=T,quote=F)

###END